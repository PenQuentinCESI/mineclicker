# mineclicker

JAVA project for CESI Brest RIL 2020 training

Mineclicker sera un jeu en ligne de genre “clicker”
(genre de jeu ou il faut cliquer pour incrémenter des valeurs permettant
d’acheter différentes augmentation pour aller plus loin dans le jeu)
se basant sur l’univers du jeu Minecraft.
Il sera développé avec un back-end sous Express.js avec une base de données MongoDB
et un front-end avec VueJS et Vuetify, le tout se basant donc sur la technologie JavaScript.


## Install the application

### Get sources with git from the repository
over SSH `git@gitlab.com:PenQuentinCESI/mineclicker.git`

over HTTPS `git clone https://gitlab.com/PenQuentinCESI/mineclicker.git`

### Import data
* go to data folder `cd data`
* run `mongod`
* run `mongo mineclicker --eval "db.dropDatabase()"`
* run `mongoimport --db mineclicker --collection mobs --file mobs.json --jsonArray`
* run `mongoimport --db mineclicker --collection biomes --file biomes.json --jsonArray`
* run `mongoimport --db mineclicker --collection items --file items.json --jsonArray`

##Run the application

###Development environnement

#### Install dependances
run `npm install`

if you encounter errors while installing bcrypt run `npm install --global --production windows-build-tools` in a powerShell as Admin
####Run the application
* run vue-cli development serveur `npm run serve`
* run mongo daemons `mongod`
* run api server `npm start`

###Production environnement

#### Install dependances
run `npm install --production`
####Configure application
In the _.env_ config file
* generate a secret key for **APP_SECRET**
* set **APP_MONGO_URI**
* set **VUE_APP_API_URL** `APP_API_URL=`
* set **NODE_ENV** `NODE_ENV=production`
####Build Front sources
run `npm build` to build front sources in _dist_ folder
####Run the application
run `npm start`