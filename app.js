const express = require('express');
const history = require('connect-history-api-fallback');
const helmet = require('helmet');
const mongoose = require('mongoose');
require('dotenv').config();

const app = express();
app.use(express.json());
app.use(helmet());

if (process.env.NODE_ENV === 'development') {
    const cors = require('cors');
    app.use(cors());
}
mongoose.connect(process.env.APP_MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log('Connected to database successfully'))
    .catch((e) => {
        console.log('Error connecting db : ' + e);
        process.exit();
    });

// TODO: Décommenter quand auth est en place pour vérifier l'auth et le token avant chaque appel
// app.use(authChecker.checkAuth);

require('./src/api/routes/user.routes.js')(app);
require('./src/api/routes/save.routes.js')(app);
require('./src/api/routes/mob.routes.js')(app);
require('./src/api/routes/biome.routes.js')(app);
require('./src/api/routes/item.routes.js')(app);
require('./src/api/routes/achievement.routes.js')(app);
require('./src/api/routes/record.routes.js')(app);

const staticFileMiddleware = express.static('dist');
app.use(staticFileMiddleware);
app.use(history({ index: 'dist/index.html' }));
app.use(staticFileMiddleware);
app.listen(8000, () => {
    console.log('Serveur is listening on port 8000')
});