const mongoose = require('mongoose');

const RecordSchema = mongoose.Schema({
    "id": { type: String, required: true },
    "name": { type: String, required: true },
    "value": { type: Number, required: true },
    "holder": { type: String, required: true },
}, {
    timestamps: true
});

module.exports = mongoose.model('Record', RecordSchema);
