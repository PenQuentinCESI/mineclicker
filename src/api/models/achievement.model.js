const mongoose = require('mongoose');

const AchievementSchema = mongoose.Schema({
    "name": { type: String, required: true },
    "image": { type: String, required: true },
    "description": { type: String, required: true },
}, {
    timestamps: true
});

module.exports = mongoose.model('Achievement', AchievementSchema);
