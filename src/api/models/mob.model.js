const mongoose = require('mongoose');

const MobSchema = mongoose.Schema({
    "id": { type: String, required: true },
    "name": { type: String, required: true },
    "biomes": {type: Array, required: true}, // [id, ...]
}, {
    timestamps: true
});

module.exports = mongoose.model('Mob', MobSchema);
