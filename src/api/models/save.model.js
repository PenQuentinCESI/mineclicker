const mongoose = require('mongoose');

const SaveSchema = mongoose.Schema({
    "stage": { type: Number, required: true },
    "owned": { type: Array, required: true }, // [{idItem: '', nb: ''}, ...]
    "achievements": { type: Array, required: true },// [id, ...]
    "xp": { type: Number, required: true },
    "diamonds": { type: Number, required: true },
    "emeralds": { type: Number, required: true },
    "kills": { type: Array, required: true }, // [{idMob: '', nb: ''}, ...]
    "user": { type: String, required: true }
}, {
    timestamps: true
});

module.exports = mongoose.model('Save', SaveSchema);
