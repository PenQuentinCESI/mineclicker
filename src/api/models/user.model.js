const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    "login": { type: String, required: true },
    "mail": { type: String, required: true },
    "password": { type: String, required: true }
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema);
