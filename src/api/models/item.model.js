const mongoose = require('mongoose');

const ItemSchema = mongoose.Schema({
    "id": { type: String, required: true },
    "name": { type: String, required: true },
    "image": {type: String, required: true},
    "description": {type: String, required: true},
    "type": {type: String, required: true},
    "typeeffect": {type: String, required: true},
    "cout": {type: Number, required: true},
    "damage": {type: Number, required: true},
    "operator": {type: String, required: true},
}, {
    timestamps: true
});

module.exports = mongoose.model('Item', ItemSchema);
