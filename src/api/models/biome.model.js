const mongoose = require('mongoose');

const BiomeSchema = mongoose.Schema({
    "id": { type: String, required: true },
    "name": { type: String, required: true },
    "order": {type: Number, required: true},
    "length": {type: Number, required: true},
    "image": {type: String, required: true},
}, {
    timestamps: true
});

module.exports = mongoose.model('Biome', BiomeSchema);
