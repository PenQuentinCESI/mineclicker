const Mob = require('../models/mob.model.js');

exports.findAll = (req, res) => {
    Mob.find({}).then(mobs => {
        res.send(mobs)
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    })
};