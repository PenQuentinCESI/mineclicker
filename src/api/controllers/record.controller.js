const Record = require('../models/record.model.js');
const User = require('../models/user.model.js');
const jwt = require('jsonwebtoken');

exports.findAll = (req, res) => {
    Record.find({}).then(record => {
        res.send(record)
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    })
};

exports.save = (req, res) => {
    // noinspection EqualityComparisonWithCoercionJS
    if (req.headers.authorization && req.headers.authorization != 'null') {
        jwt.verify(req.headers.authorization, process.env.APP_SECRET, (e, decoded) => {
            if (e) {
                console.error(e);
                res.status(500).send();
            } else {
                if (!req.body) {
                    return res.status(400).send("Record content can not be empty");
                }
                User.findOne({ _id: decoded.id }).then(user => {
                    if (!user) {
                        res.status(401).send('Invalid token');
                    } else {
                        req.body.holder = user.login;
                        Record.replaceOne({ id: req.body.id }, req.body, { upsert: true }).then(() => {
                            res.send('saved');
                            console.log("ok");
                        }).catch(err => { console.error(err) });
                    }
                });

            }
        })
    } else {
        res.status(401).send();
    }
};