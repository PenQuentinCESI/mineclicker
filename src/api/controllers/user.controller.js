const User = require('../models/user.model.js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

exports.create = (req, res) => {
    if (!req.body) {
        return res.status(400).send("User content can not be empty");
    }
    let userData = req.body;
    User.findOne({login: req.body.login}).then(user => {
        if (user) {
            res.status(403).send('Login already used');
        } else {
            userData.password = bcrypt.hashSync(userData.password, 8);
            const user = new User(userData);
            user.save().then(data => {
                let token = jwt.sign({id: data.id}, process.env.APP_SECRET, {expiresIn: 86400});
                res.status(200).send({auth: true, token: token, user: data});
            }).catch(err => {
                console.error(err.message);
                res.status(500).send("Some error occurred while creating the User.");
            });
        }
    });
};
exports.connect = (req, res) => {
    User.findOne({login: req.body.login}).then(user => {
        if (user && bcrypt.compareSync(req.body.password, user.password)) {
            let token = jwt.sign({id: user.id}, process.env.APP_SECRET, {expiresIn: 86400});
            res.status(200).send({auth: true, token: token, user: user});
        } else {
            res.status(401).send('Connection failed: wrong login or password')
        }
    }).catch(err => {
        console.error(err);
        res.status(500).send("Some error occurred while connecting the User.");
    });
};