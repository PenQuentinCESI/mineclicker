const User = require('../models/user.model.js');
const Save = require('../models/save.model.js');
const jwt = require('jsonwebtoken');

exports.save = (req, res) => {
    // noinspection EqualityComparisonWithCoercionJS
    if (req.headers.authorization && req.headers.authorization != 'null') {
        jwt.verify(req.headers.authorization, process.env.APP_SECRET, (e, decoded) => {
            if (e) {
                console.error(e);
                res.status(500).send();
            } else {
                if (!req.body) {
                    return res.status(400).send("Save content can not be empty");
                }
                User.findOne({_id: decoded.id}).then(user => {
                    if (!user) {
                        res.status(401).send('Invalid token');
                    } else {
                        req.body.user = user._id;
                        Save.replaceOne({user: user._id}, req.body, {upsert: true}).then(() => {
                            res.send('saved');
                        }).catch(err => { console.error(err)});
                    }
                });
            }
        })
    } else {
        res.status(401).send();
    }
};

exports.load = (req, res) => {
    // noinspection EqualityComparisonWithCoercionJS
    if (req.headers.authorization && req.headers.authorization != 'null') {
        jwt.verify(req.headers.authorization, process.env.APP_SECRET, (e, decoded) => {
            if (e) {
                console.error(e);
                res.status(500).send();
            } else {
                if (!req.body) {
                    return res.status(400).send("Save content can not be empty");
                }
                User.findOne({_id: decoded.id}).then(user => {
                    if (!user) {
                        res.status(401).send('Invalid token');
                    } else {
                        req.body.user = user._id;
                        Save.findOne({user: user._id}).then((data) => {
                            if(data){

                                res.send(data);
                            }
                            else{
                                res.status(404).send('No save');
                            }
                        }).catch(err => { console.error(err)});
                    }
                });
            }
        })
    } else {
        res.status(401).send();
    }
};