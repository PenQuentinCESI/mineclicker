const Biome = require('../models/biome.model.js');
exports.findAll = (req, res) => {
    Biome.find(null,null, {sort: {order: 1}}).populate("mob").then(biomes => {
        res.send(biomes)
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    })
};

