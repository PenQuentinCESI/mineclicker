const Item = require('../models/item.model.js');

exports.findAll = (req, res) => {
    let datas = new Object();
    Item.find({type: 'equipment'}, null, {sort: {order: 1}}).then(items => {
        datas.equipments = items;
        Item.find({type: 'enchantment'}, null, {sort: {order: 1}}).then(items => {
            datas.enchantments = items;
            Item.find({type: 'partner'}, null, {sort: {order: 1}}).then(items => {
                datas.partners = items;
                res.send(datas);
            }).catch(err => {
                console.error(err);
                res.status(500).send();
            })
            //res.send(items);
        }).catch(err => {
            console.error(err);
            res.status(500).send();
        })
        //res.send(items);
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    })
};