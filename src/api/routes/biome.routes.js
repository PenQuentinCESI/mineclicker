module.exports = (app) => {
    const biome = require('../controllers/biome.controller.js');
    app.get('/biome/all', biome.findAll);
};