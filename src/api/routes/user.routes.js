module.exports = (app) => {
    const user = require('../controllers/user.controller.js');
    app.post('/user/create', user.create);
    app.post('/user/login', user.connect);
}
