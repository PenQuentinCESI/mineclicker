module.exports = (app) => {
    const achievement = require('../controllers/achievement.controller.js');
    app.get('/achievement/all', achievement.findAll);
};