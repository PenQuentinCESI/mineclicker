module.exports = (app) => {
    const record = require('../controllers/record.controller.js');
    app.get('/record/all', record.findAll);
    app.post('/record/save', record.save);
};