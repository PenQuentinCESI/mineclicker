module.exports = (app) => {
    const mob = require('../controllers/mob.controller.js');
    app.get('/mob/all', mob.findAll);
};