module.exports = (app) => {
    const user = require('../controllers/save.controller.js');
    app.post('/save', user.save);
    app.get('/load', user.load);
};
