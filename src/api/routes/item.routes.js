module.exports = (app) => {
    const item = require('../controllers/item.controller.js');
    app.get('/item/all', item.findAll);
}
