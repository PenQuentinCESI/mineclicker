const jwt = require('jsonwebtoken');
const config = require('../../../../PFR/madera2020/config/app.config');
exports.checkAuth = (req, res, next) => {
    if (req.path !== '/user/connect') {
        if (req.headers.authorization && jwt.verify(req.headers.authorization, config.secret)) {
            next();
        } else {
            res.status(403).send('Not connected');
        }
    } else {
        next();
    }
};
