import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    mobileNavbarActive: 'combat',
    classement: false,
    shop: false,
    snackbar: {
      active: false,
      color: '',
      text: ''
    },
    achievementSnack: {
      active: false,
      color: 'grey',
      text: '',
      image: 'Wooden_Sword.png'
    },
    loggedIn: false,
    mobs: [],
    biomes: [],
    items: [],
    dpcDamage: 1,
    dpsDamage: 0,
    dropRateDiamonds: 80,
    listEquipments: [],
    listPartners: [],
    listEnchantments: [],
    stageInfo: {
      lvl: 1,
      lvlname: 'Foret de bouleaux',
      urllvl: 'Birch_Forest.png',
      mob: {
        id: "Fox",
        name: "Renard",
        hp: 10,
        hpleft: 10,
        mobKilled: false,

      }
    },
    xp: 0,
    diamonds: 0,
    achievements: [],
    records: []
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
